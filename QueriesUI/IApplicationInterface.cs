﻿using System.Threading.Tasks;

namespace QueriesUI
{
    interface IApplicationInterface
    {
        Task RunAsync();
    }
}