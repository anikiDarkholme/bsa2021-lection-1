﻿using ProjectsAccess.DataAccess.IRepositories;
using ProjectsAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectsAccess.DataAccess.IRepositories
{
    public interface ITeamRepository : IRepository<Team>
    {
    }
}
